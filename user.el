;; Env path
(defun set-exec-path-from-shell-PATH ()
  (let ((path-from-shell (shell-command-to-string "$SHELL -i -c 'echo $PATH'")))
    (setenv "PATH" path-from-shell)
    (setq exec-path (split-string path-from-shell path-separator))))

;; Add load paths here
(add-to-list 'load-path "~/.emacs.d/vendor")
(add-to-list 'load-path "~/.emacs.d/themes")
(add-to-list 'load-path "~/.emacs.d/themes/emacs-color-theme-solarized")
(add-to-list 'load-path "~/.emacs.d")

;; Themes
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/emacs-color-theme-solarized")
(load-theme 'solarized-dark t)

;; Flyspell often slows down editing so it's turned off
(remove-hook 'text-mode-hook 'turn-on-flyspell)

;; hippie expand - don't try to complete with file names
(setq hippie-expand-try-functions-list (delete 'try-complete-file-name hippie-expand-try-functions-list))
(setq hippie-expand-try-functions-list (delete 'try-complete-file-name-partially hippie-expand-try-functions-list))

(setq ido-use-filename-at-point nil)

;; Save here instead of littering current directory with emacs backup files
(setq backup-directory-alist `(("." . "~/.saves")))

;; Module for custom time inserts
(require 'insert-time)
(define-key global-map [(control c)(d)] 'insert-date-time)
(define-key global-map [(control c)(control v)(d)] 'insert-personal-time-stamp)
(global-set-key (kbd "C-c C-w") 'ethan-wspace-clean-all) ; Alt+a
(global-set-key (kbd "C-c C-u") 'insert-date-time) ; Alt+a

;; Make org-mode work with files ending in .org
(require 'org)
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
(setq org-insert-mode-line-in-empty-file t)

;; Query each replacement
(defalias 'qrr 'query-replace-regexp)

;; Use ibuffer as default buffer switcher
(defalias 'list-buffers 'ibuffer)

;; Alternative to M-x
(global-set-key "\C-x\C-m" 'execute-extended-command)
(global-set-key "\C-c\C-m" 'execute-extended-command)

;; Alternative to C-w
(global-set-key "\C-x\C-k" 'kill-region)
(global-set-key "\C-c\C-k" 'kill-region)

;; Don't use the backspace key
(global-set-key "\C-w" 'backward-kill-word)

;; Use the power of regular expressions
(global-set-key "\M-s" 'isearch-forward-regexp)
(global-set-key "\M-r" 'isearch-backward-regexp)

;; Shortcuts for easily en-/disabling case-sensitive search & more
(add-hook 'isearch-mode-hook
          (function
           (lambda ()
             (define-key isearch-mode-map "\C-h" 'isearch-mode-help)
             (define-key isearch-mode-map "\C-t" 'isearch-toggle-regexp)
             (define-key isearch-mode-map "\C-c" 'isearch-toggle-case-fold)
             (define-key isearch-mode-map "\C-j" 'isearch-edit-string))))

;; Use spaces instead of tabs
(setq-default indent-tabs-mode nil)

;(setq backup-inhibited t ) ; disable backup
;(setq auto-save-default nil) ; disable auto save

; (setq tramp-default-method "pscp")

;; Good stuff for keeping for keeping your files clean, convert tabs
;; to spaces and remove whitespace cluttered lines
;; (add-to-list 'load-path (expand-file-name "~/.emacs.d/ethan-wspace"))
(require 'ethan-wspace)
(global-ethan-wspace-mode 1)

(put 'downcase-region 'disabled nil)

;; Show lines longer than 80 characters
(require 'column-enforce-mode)
(global-column-enforce-mode t)
