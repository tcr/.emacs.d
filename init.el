;; (when
;;     (load
;;      (expand-file-name "~/.emacs.d/vendor/package.el")))

(require 'package)
(add-to-list 'package-archives
             '("marmalade" . "http://marmalade-repo.org/packages/") t)
(add-to-list 'package-archives
             '("tromey" . "http://tromey.com/elpa/") t)
(package-initialize)

(when (not package-archive-contents)
  (package-refresh-contents))

;; user stuff is kept here
(load "~/.emacs.d/user.el")
